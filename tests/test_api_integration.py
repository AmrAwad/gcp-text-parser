import unittest
from app import get_email_address, get_domain_name, add_contact


class TestApiIntegration(unittest.TestCase):
    def test_get_email_address(self):
        result = get_email_address(domain="impossiblefoods.com",
                                   first_name="Nikki", last_name="Mostafavi")

        self.assertEqual('nikki.mostafavi@impossiblefoods.com', result)

    def test_get_domain_name(self):
        result = get_domain_name(company_name="Exact Sciences")

        self.assertEqual('exactsciences.com', result)

    def test_add_contact(self):
        self.assertTrue(add_contact("amr", "khir", "amro.khir@gmail.com", "Engineer"))
