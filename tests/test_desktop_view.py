import os
import unittest
from app import parse_desktop_view
import test_config as config


class DesktopView(unittest.TestCase):
    def test_desktop_view_1(self):
        with open(os.path.join(config.TEST_TXT_DIR, 'desktop_1.txt'), 'r') as fh:
            full_text = fh.read()

        parsed = parse_desktop_view(full_text)
        self.assertEqual(16, len(parsed))

        self.assertEqual(parsed[0]['name'], 'Eva Chan, CTP')
        self.assertEqual(parsed[0]['company'], 'Cathay Bank')
        self.assertEqual(parsed[0]['position'], 'Vice President & Treasury Risk Manager')
        self.assertEqual(parsed[0]['city'], 'Greater Los Angeles Area')

    def test_desktop_view_2(self):
        with open(os.path.join(config.TEST_TXT_DIR, 'desktop_2.txt'), 'r') as fh:
            full_text = fh.read()

        parsed = parse_desktop_view(full_text)
        self.assertEqual(24, len(parsed))

    def test_desktop_view_3(self):
        with open(os.path.join(config.TEST_TXT_DIR, 'desktop_3.txt'), 'r') as fh:
            full_text = fh.read()

        parsed = parse_desktop_view(full_text)
        self.assertEqual(17, len(parsed))
