# Google Cloud Vision Parser

This project aims to extract info from Linkedin Sales Navigator views using Google Cloud Vision OCR.


The script currently supports 3 views from sales navigator:
1. Mobile view:
- use the `-t mobile` option.
- Returns: [name, position, company]
- example in `imgs\moblie_n.png`

2. Company view:
- use the `-t company` option.
- Returns: [Name, Field, City, EmployeeCount]
- example in `imgs\company_n.png`

3. People Desktop view:
- use the `-t desktop` option.
- Returns: [name, company, position, city]
- example in `imgs\people_n.png`


# Setup

1. Install the requirements in a python environment
```
pip install -r requirements.txt
```

2. Set up Google cloud credentials by following the steps in [this guide](https://cloud.google.com/vision/docs/quickstart#set_up_a_google_cloud_vision_api_project), rename the json file to `secret.json` and put it in the same place as the script.

3. Add the API keys in the file `app/api_config.py`, like this:
```
API_HUNTER_API_KEY = "key"
HUBSPOT_API_KEY = "key2"
```

# Running

The script has a command line interface, to get details about it:
```
> python googlecptext.py -h
usage: googlecptext.py [-h] [-o OUT_FILE] [-t {mobile,desktop}] image

positional arguments:
  image                 The image for text detection.

optional arguments:
  -h, --help            show this help message and exit
  -o OUT_FILE, --out_file OUT_FILE
                        Output file to save the results to.
  -t {mobile,desktop}, --type {mobile,desktop}
                        Type of image.
```

Here's a simple example running the script:
```
>python googlecptext.py imgs\mobile_2.png
INFO - Initializing Google Client
INFO - loading image from: imgs\mobile_2.png
INFO - Processing Image with Google vision client
INFO - Parsing response
INFO - Processing Completed
{"Name": "Tracy W.", "Position": "Treasury Manager", "Company": "Symantec"}
{"Name": "Pamela McElwee", "Position": "Senior Treasury Manager", "Company": "Amtrak"}
{"Name": "Elena Kong", "Position": "Treasury Manager", "Company": "Matson, Inc."}
{"Name": "Rob Rummer, CTP", "Position": "Global Treasury Manager", "Company": "OMNOVA Solutions"}
{"Name": "Randy Cardon", "Position": "Manager Treasury", "Company": "San Antonio Water System"}
{"Name": "Preston Moerke", "Position": "None", "Company": "None"}
```
