import os
import unittest
from app import parse_mobile_view
import test_config as config


class MobileView(unittest.TestCase):
    def test_mobile_view_1(self):
        with open(os.path.join(config.TEST_TXT_DIR, 'mobile_1.txt'), 'r') as fh:
            full_text = fh.read()

        parsed = parse_mobile_view(full_text)

        self.assertEqual(6, len(parsed))

        self.assertEqual('Oswald Lee', parsed[0]['Name'])
        self.assertEqual('Treasury Services Manager', parsed[0]['Position'])
        self.assertEqual('FITECH Consultants', parsed[0]['Company'])

    def test_mobile_view_2(self):
        with open(os.path.join(config.TEST_TXT_DIR, 'mobile_2.txt'), 'r') as fh:
            full_text = fh.read()

        parsed = parse_mobile_view(full_text)

        self.assertEqual(parsed[1]['name'], 'Anthony Car...')
        self.assertEqual(parsed[1]['company'], 'Treasury Strategies, a di...')
        self.assertEqual(parsed[1]['position'], 'Managing Director')
        self.assertEqual(parsed[1]['city'], 'Greater Chicago Area')

    def test_mobile_view_2_correct_detection(self):
        with open(os.path.join(config.TEST_TXT_DIR, 'mobile_2.txt'), 'r') as fh:
            full_text = fh.read()

        parsed = parse_mobile_view(full_text)

        self.assertEqual(25, len(parsed))

    def test_mobile_view_2_handling_missing_seperators(self):
        with open(os.path.join(config.TEST_TXT_DIR, 'mobile_2.txt'), 'r') as fh:
            full_text = fh.read()

        parsed = parse_mobile_view(full_text)

        self.assertEqual(parsed[1]['name'], 'Ed Barrie')
        self.assertEqual(parsed[1]['company'], 'Tableu Software')
        self.assertEqual(parsed[1]['position'], 'Senior Director, Treasury')
        self.assertEqual(parsed[1]['city'], 'Spokane, Washington Area')
