import re
from collections import OrderedDict


def parse_mobile_view(full_text):
    ''' Parse the text from the mobile view into a list of people

    Arguments:
        full_text {str} -- full text acquired from an image

    Returns:
        list(OrderedDict) -- A list of ordered dicts of people
    '''
    parsed = list()
    lines = [line for line in full_text.split('\n') if line]
    for idx, line in enumerate(lines):
        if '2nd' in line or '3rd' in line or '1st' in line:
            if '|' in line:
                # View 2
                sep = None
                if '1st' in line:
                    sep = '1st'
                elif '2nd' in line:
                    sep = '2nd'
                elif '3rd' in line:
                    sep = '3rd'
                if sep:
                    parsed.append(OrderedDict({
                        'name': line.split(sep)[0][:-3].strip(),
                        'company': line.split(sep)[1][3:].replace('in', '').strip(),
                        'position': lines[idx+1] if idx+1 < len(lines) else 'None',
                        'city': lines[idx+3] if idx+3 < len(lines) else 'None'
                    }))
            else:
                # View 1
                parsed.append(OrderedDict({
                    'name': line[:-7].strip(),
                    'position': lines[idx+1] if len(lines) > idx+1 else 'None',
                    'company': lines[idx+2] if len(lines) > idx+2 else 'None'
                }))
    return parsed


def parse_company_view(full_text):
    ''' Parse the text from the company view into a list of companies

    Arguments:
        full_text {str} -- full text acquired from an image

    Returns:
        list(OrderedDict) -- A list of ordered dicts of companies
    '''
    parsed = list()
    lines = [line for line in full_text.split('\n') if line]
    for idx, line in enumerate(lines):
        if re.match(r'\d+-\d+ employees', line):
            parsed.append(OrderedDict({
                'name': lines[idx-3],
                'field': lines[idx-2],
                'city': lines[idx-1],
                'employeeCount': line
            }))
    return parsed


def parse_desktop_view(full_text):
    ''' Parse the text from the desktop view into a list of people

    Arguments:
        full_text {str} -- full text acquired from an image

    Returns:
        list(OrderedDict) -- A list of ordered dicts of people
    '''
    parsed = list()
    lines = [line for line in full_text.split('\n') if line]
    for idx, line in enumerate(lines):
        if '2nd' in line or '3rd' in line:
            if idx+1 < len(lines):
                if len(lines[idx+1].split(' at ')) < 2:
                    lines[idx+1] = lines[idx+1] + ' ' + lines[idx+2]
                    lines[idx+3] = lines[idx+4] if idx+4 < len(lines) else 'None'

            person = OrderedDict({
                'name': line[:-4].strip().split(",")[0].replace(" 3rd", "").replace(" 1st", "")
                                                       .replace(" 2nd", "").replace(" in", ""),
                'company': lines[idx+1].split(' at ')[1].strip() if idx+1 < len(lines) else 'None',
                'position': lines[idx+1].split(' at ')[0].strip() if idx+1 < len(lines) else 'None',
                'city': lines[idx+3] if idx+3 < len(lines) else 'None'
            })

            # Fix for company names over 2 lines
            if len(lines) > idx+2 and 'month' not in lines[idx+2].lower():
                person['company'] += ' ' + lines[idx+2]

            if 'month' in person['city']:
                person['city'] = lines[idx+4] if idx+4 < len(lines) else 'None'

            parsed.append(person)
    return parsed
