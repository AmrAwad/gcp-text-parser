import json
import requests
from .api_config import API_HUNTER_API_KEY, HUBSPOT_API_KEY


def get_domain_name(company_name):
    QUERY_URL = "https://autocomplete.clearbit.com/v1/companies/suggest"
    response = requests.get(QUERY_URL, params={"query": company_name})
    if ((response.status_code != 200) or (len(response.json()) == 0)):
        return 'Not Identified'
    else:
        return response.json()[0]['domain']


def get_email_address(domain, first_name, last_name):
    QUERY_URL = " https://api.hunter.io/v2/email-finder"
    response = requests.get(QUERY_URL, params={"domain": domain,
                                               "first_name": first_name,
                                               "last_name": last_name,
                                               "api_key": API_HUNTER_API_KEY})
    if ((response.status_code != 200) or (response.json()['data']['email'] is None)):
        return 'Not Found'
    else:
        return response.json()['data']['email']


def add_contact(first_name, last_name, email, position):
    POST_URL = "https://api.hubapi.com/contacts/v1/contact?hapikey={}".format(HUBSPOT_API_KEY)
    properties = {'firstname': first_name,
                  'lastname': last_name,
                  'email': email,
                  'jobtitle': position}

    properties_list = list()
    for name, value in properties.items():
        properties_list.append({"property": name, "value": value})

    response = requests.post(POST_URL,
                             data=json.dumps({"properties": properties_list}),
                             headers={'Content-Type': 'application/json'})

    if response.status_code == 200:
        return True
    return False
