import io
import logging
from google.cloud import vision
from google.cloud.vision import types


def process_image(image_path):
    ''' Process an image with Google's document text detection

    Arguments:
        image_path {str} -- path to the image to be processed

    Returns:
        str -- the full text inside the image
    '''
    logging.info("Initializing Google Client")
    client = vision.ImageAnnotatorClient()

    logging.info("loading image from: {}".format(image_path))
    with io.open(image_path, 'rb') as image_file:
            content = image_file.read()
    image = types.Image(content=content)

    logging.info("Processing Image with Google vision client")
    response = client.document_text_detection(image=image)
    full_text = response.text_annotations[0].description
    return full_text
