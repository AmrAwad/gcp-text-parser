# -*- coding: utf-8 -*-
import os
import csv
import logging
import argparse
import json
from app import (
    process_image, parse_company_view, parse_desktop_view, parse_mobile_view,
    get_domain_name, get_email_address, add_contact
)


if __name__ == '__main__':
    # Parsing command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('image', help='The image for text detection.', type=str)
    parser.add_argument('-o', '--out_file', help='Output file to save the results to.', default=0)
    parser.add_argument('-t', '--type', choices=['mobile', 'company', 'desktop'],
                        help='Type of image.', default='mobile', type=str)
    args = parser.parse_args()

    parser = argparse.ArgumentParser()

    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.join(os.path.dirname(__file__),
                                                                "secret.json")

    logging.basicConfig(level=logging.INFO, format="%(levelname)s - %(message)s")

    full_text = process_image(image_path=args.image)
    logging.info("Parsing response")

    # Parse text according to image type
    if args.type == 'mobile':
        parsed = parse_mobile_view(full_text=full_text)
    elif args.type == 'company':
        parsed = parse_company_view(full_text=full_text)
    elif args.type == 'desktop':
        parsed = parse_desktop_view(full_text=full_text)

    logging.info("Image Processing Completed: {} people detected".format(len(parsed)))

    logging.info("Getting domain & email information")

    for person in parsed:
        person['firstname'] = person['name'].split()[0]
        person['lastname'] = person['name'].split()[-1]
        person['domain'] = get_domain_name(company_name=person['company'])
        person['crm_success'] = 'N/A'
        person['email'] = 'N/A'
        if person['domain'] != 'Not Identified':
            person['email'] = get_email_address(domain=person['domain'],
                                                first_name=person['firstname'],
                                                last_name=person['lastname'])
            if person['email'] != 'Not Found':
                person['crm_success'] = add_contact(first_name=person['firstname'],
                                                    last_name=person['lastname'],
                                                    email=person['email'],
                                                    position=person['position'])

    # Output the results
    if args.out_file == 0:
        for p in parsed:
            print(json.dumps(p))
    else:
        with open(args.out_file, 'w', newline='') as fh:
            fieldnames = ['firstname', 'lastname', 'company', 'domain', 'email',
                          'position', 'crm_success']
            writer = csv.DictWriter(fh, fieldnames=fieldnames, extrasaction='ignore')
            writer.writeheader()

            for person in parsed:
                writer.writerow(person)
